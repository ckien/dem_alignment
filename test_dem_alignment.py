import numpy as np
import matplotlib.pyplot as plt
import os.path as osp
import random
import seaborn as sns

# local modules
import dem_alignment 
import utilities as util # requires gdal to convert geotiff to np raster 

try:
    plt.style.use('seaborn')
except:
    sns.set()
    sns.axes_style({'ytick.left': False, 'ytick.right': False, 
                    'xtick.bottom': False, 'xtick.top': False})
    
def test_dem_alignment1():    
    '''
    test dem_alignment by shifting a DEM artificially by full numbers of gridcells
    '''

    # load test DEM
    Z1 = np.load('./data/test/test_ras.npz')['dem']
          
    px_res = 5.0 # pixel spacing in m
     
    # create a shifted DEM - move N pixels to the North and E pixels to the East
    # U meters up
    N = 4 # pixels to the north
    E = -2 # pixels to the east
    U = -3 # meters up 
        
    # move and mask
    # axis=1 for east-west, axis=0 for north-south direction
    Z2 = np.roll(np.roll(Z1, E, axis=1), -N, axis=0)   

    # clip dems around the corners where we have nans
    Z2 = Z2[abs(N):-1 - abs(N), abs(E):-1 - abs(E)]
    Z1 = Z1[abs(N):-1 - abs(N), abs(E):-1 - abs(E)]
       
    # add elevation diff
    Z2 = Z2 + U
    
    #--------------------------------------------------------------------------
    
    plt.close('all')
    
    # create an instance of the class
    dem_aligner = dem_alignment.DEM_Aligner(Z1, Z2, px_res)
    
    # fitting_method = ['leastsq', 'least_squares', 'differential_evolution', 
    #                    'basinhopping', 'ampgo', 'nelder', 'lbfgsb', 'powell']    

    # interpolation_methods == ['griddata', 'interp2d', 'RectBivariateSpline']    
    
    # run alginment - since there are no nans in the elevation model, we can use
    # the fast interplators 'interp2d' or 'RectBivariateSpline'
    dem_aligner.run_alignment(max_iterations=10, delta_rmse=0.001, 
                              fitting_method='differential_evolution', 
                              interpolation_method='RectBivariateSpline')
    
    [fig, ax] = dem_aligner.plot_results()
    
    Z2_shifted = dem_aligner.Z2
    plt.figure()
    plt.imshow(Z2_shifted, cmap='terrain', interpolation='None')
    
   
def test_dem_alignment2():
    '''
    test dem_alignment by shifting a DEM artificially by random numbers of meters
    '''

    Z1 = np.load('./data/test/test_ras.npz')['dem']
    
    px_res = 5.0
    
    # create an instance of the class
    dem_aligner = dem_alignment.DEM_Aligner(Z1, px_res=px_res)
    
    # randomly sample shifts
    x_shift_prescribed = random.sample(np.arange(-20, 20, 0.1), 1)[0]
    y_shift_prescribed = random.sample(np.arange(-20, 20, 0.1), 1)[0]
    z_shift_prescribed = random.sample(np.arange(-10, 10, 0.1), 1)[0]
    
    # apply these shifts to the original DEM
    Z2 = dem_aligner.shift_DEM(x_shift=x_shift_prescribed, y_shift=y_shift_prescribed, 
                               z_shift=z_shift_prescribed, method='griddata')
    
    # plt.figure()
    # plt.imshow(Z2, interpolation=None)
          
    plt.close('all')
    
    # create an instance of the class, using original and shifted DEM as input
    dem_aligner = dem_alignment.DEM_Aligner(Z1, Z2, px_res)
    
    # run alignment
    # delta_rmse of -1 makes sure all 10 iterations are conducted
    # delta_rmse of 0 stops iterations in case the rmse gets worse     
    dem_aligner.run_alignment(max_iterations=10, delta_rmse=-1.0, 
                              fitting_method='differential_evolution', 
                              interpolation_method='griddata')
    
    [fig, ax] = dem_aligner.plot_results() # cbarlim=5
           
    # compare prescribed and corrected shifts (ratio should be -1.0)
    print '----------------------------------------------------------'
    print 'ratio x: {}'.format(x_shift_prescribed / dem_aligner.best_x_shift)
    print 'ratio y: {}'.format(y_shift_prescribed / dem_aligner.best_y_shift)
    print 'ratio z: {}'.format(z_shift_prescribed / dem_aligner.best_z_shift)
     
def test_suicide_basin():
    
    workspace = './data/suicide_basin'
        
    # load original DEMs and mask
    [Z1, cols, rows, transform, projection] = util.rastertonumpy(osp.join(workspace, 'reference.tif'))
    Z2 = util.rastertonumpy(osp.join(workspace, 'WV_20130728.tif'))[0]
    mask = util.rastertonumpy(osp.join(workspace, 'mask.tif'))[0].astype('float32')
    cellsize = transform[1]
        
    Z1[Z1<0] = np.nan
    Z2[Z2<0] = np.nan
    mask[mask==0] = np.nan
    mask[mask>0] = 1
    
    # plt.imshow(Z1)
    # plt.imshow(Z2)
    # plt.imshow(mask)
    
    # create an instance of the class, using original and shifted DEM as input
    dem_aligner = dem_alignment.DEM_Aligner(Z1, Z2, cellsize, mask)   
    
    # run alignment 
    # delta_rmse of -1 makes sure all 10 iterations are conducted
    # delta_rmse of 0 stops iterations in case the rmse gets worse
    dem_aligner.run_alignment(max_iterations=5, delta_rmse=-1.0, 
                              fitting_method='differential_evolution', 
                              interpolation_method='griddata')
    
    # plot results, set limit of colorbars in dh map to +- 3 m
    [fig, ax] = dem_aligner.plot_results(cbarlim=3)
    
    # convert shifted DEM back to geotiff
    util.numpytoraster(osp.join(workspace, 'dem2_shifted.tif'),
                       dem_aligner.Z2, cols, rows, transform, projection)
                       
def test_suicide_basin_WV():
    
    workspace = './data/suicide_basin_WV'
        
    # load original DEMs and mask
    [Z1, cols, rows, transform, projection] = util.rastertonumpy(osp.join(workspace, 'dem1.tif'))
    Z2 = util.rastertonumpy(osp.join(workspace, 'dem2.tif'))[0]
    mask = util.rastertonumpy(osp.join(workspace, 'mask.tif'))[0].astype('float32')
    cellsize = transform[1]
        
    Z1[Z1<0] = np.nan
    Z2[Z2<0] = np.nan
    mask[mask==0] = np.nan
    mask[mask>0] = 1
    
    # plt.imshow(Z1)
    # plt.imshow(Z2)
    # plt.imshow(mask)
    
    # create an instance of the class, using original and shifted DEM as input
    dem_aligner = dem_alignment.DEM_Aligner(Z1, Z2, cellsize, mask)   
    
    # run alignment 
    # delta_rmse of -1 makes sure all 10 iterations are conducted
    # delta_rmse of 0 stops iterations in case the rmse gets worse
    dem_aligner.run_alignment(max_iterations=5, delta_rmse=-1.0, 
                              fitting_method='differential_evolution', 
                              interpolation_method='griddata')
    
    # plot results, set limit of colorbars in dh map to +- 3 m
    [fig, ax] = dem_aligner.plot_results(cbarlim=3)
    
    # convert shifted DEM back to geotiff
    util.numpytoraster(osp.join(workspace, 'dem2_shifted.tif'),
                       dem_aligner.Z2, cols, rows, transform, projection)