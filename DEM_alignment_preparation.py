#-------------------------------------------------------------------------------
# Christian Kienholz, University of Alaska Southeast, 3/2018
#-------------------------------------------------------------------------------

'''
This script prepares DEMs and mask for the subsequent DEM alignment 
where they need to have identical cellsize and extent.

Requires ArcGIS/arcpy

Set-up to be run from the "DEM_alignment_preparation" toolbox
'''


# import modules
import arcpy
arcpy.CheckOutExtension('Spatial')
import os.path as osp

# Script arguments
DEM1 = arcpy.GetParameterAsText(0)
DEM2 = arcpy.GetParameterAsText(1)
AOI = arcpy.GetParameterAsText(2)
QA = arcpy.GetParameterAsText(3)    
workspace = arcpy.GetParameterAsText(4)
DEM1_masked = arcpy.GetParameterAsText(5)
DEM2_masked = arcpy.GetParameterAsText(6)
mask_clipped = arcpy.GetParameterAsText(7)

# DEM1 = r'A:\4_Mendenhall\Drone_processing\suiB_2018_07_24_dem_final.tif'
# DEM2 = r'B:\4_Mendenhall\DEMs\SFM_DGGS_2015\SuicideBasin_2015_DSM_WGS84UTM8N.tif'
# AOI = r'A:\4_Mendenhall\DEM_alignment_2015\AOI_2015.shp'
# QA = r'A:\4_Mendenhall\DEM_alignment_2015\QA_2015.shp'
# workspace = r'A:\4_Mendenhall\workspace'
# DEM1_masked = 'dem1.tif'
# DEM2_masked = 'dem2.tif'
# mask_clipped = 'mask.tif'

# Concatenate actual paths for output products
DEM1_masked = osp.join(workspace, DEM1_masked)
DEM2_masked = osp.join(workspace, DEM2_masked)
mask_clipped = osp.join(workspace, mask_clipped)

# Paths for intermediate results 
AOI_raster = osp.join(workspace, 'AOI_raster.tif')
AOI_raster_copy = osp.join(workspace, 'AOI_raster_copy.tif')
DEM2_resampled = osp.join(workspace, 'DEM2_resampled.tif')
QA_raster = osp.join(workspace, 'QA_raster.tif')

arcpy.env.overwriteOutput = 1
arcpy.env.snapRaster = DEM1
arcpy.env.cellSize = DEM1

#------------------------------------------------

# Calculate Field for AOI shapefile
arcpy.CalculateField_management(AOI, 'Id', '0', 'PYTHON_9.3')

# Convert AOI to raster
arcpy.FeatureToRaster_conversion(AOI, 'Id', AOI_raster, cell_size=DEM1)

# Calculate Field of QA shapefile
arcpy.CalculateField_management(QA, 'Id', '1', 'PYTHON_9.3')

# Convert QA shapefile to raster
arcpy.FeatureToRaster_conversion(QA, 'Id', QA_raster, DEM1)

# Copy AOI Raster for subsequent mosaicing
arcpy.CopyRaster_management(AOI_raster, AOI_raster_copy)

# Mosaic QA raster on top of AOI raster
arcpy.Mosaic_management(QA_raster, AOI_raster_copy)

# Extract mosaic by Mask
arcpy.gp.ExtractByMask_sa(AOI_raster_copy, AOI_raster, mask_clipped)

#------------------------------------------------

# Extract DEM1 by Mask
arcpy.gp.ExtractByMask_sa(DEM1, AOI_raster, DEM1_masked)

# Resample DEM2 to resolution of DEM1
arcpy.Resample_management(DEM2, DEM2_resampled, arcpy.env.cellSize, 'BILINEAR')

# Extract DEM2 by Mask
arcpy.gp.ExtractByMask_sa(DEM2_resampled, AOI_raster, DEM2_masked)

#------------------------------------------------
# Delete intermediate results
for ras in [AOI_raster, AOI_raster_copy, DEM2_resampled, QA_raster]:
    arcpy.Delete_management(ras)