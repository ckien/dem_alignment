import numpy as np
import scipy.io
import os.path as osp

try:
    import gdal
except ImportError:
    print 'gdal not available, rastertonumpy and numpytoraster functions will not work'
      
def rastertonumpy(geotiff):
    '''
    Converts geotiff to numpyarray and returns the corresponding gdal 
    object (contains info to convert the array back to a geotiff)
    '''
    rasterobj = gdal.Open(geotiff)
    array = rasterobj.ReadAsArray()
    
    # collect key features from raster object
    cols = rasterobj.RasterXSize
    rows = rasterobj.RasterYSize
    # contains (originX, pixelWidth, b, originY, d, pixelHeight)
    geotransform = rasterobj.GetGeoTransform()
    projection = rasterobj.GetProjection()
    
    # close object
    rasterobj = None
    
    return [array, cols, rows, geotransform, projection]
    

def numpytoraster(path, array, cols, rows, geotransform, projection):
    '''
    Converts numpy array back to geotiff

    parameters
    ----------
    path: 
        path to the new geotiff
    array:
        numpy array
    cols:
        nr of columns
    rows:
        nr of rows
    geotransform:
        contains (originX, pixelWidth, b, originY, d, pixelHeight)
    projection:
        projection
    '''
       
    driver = gdal.GetDriverByName('GTiff')
    # create empty geotiff with one band 
    geotiff = driver.Create(path, cols, rows, 1, gdal.GDT_Float32)
    
    # write array
    outband = geotiff.GetRasterBand(1)
    outband.WriteArray(array)
    
    geotiff.SetGeoTransform(geotransform)
    geotiff.SetProjection(projection)
    geotiff.FlushCache()
  
    #return geotiff
        
def import_test_dem():
    '''
    import geotiff and convert to .npz and .mat files
    '''
    
    workspace = './data/test'

    dem = rastertonumpy(osp.join(workspace, 'test_dem.tif'))[0]
    slope = rastertonumpy(osp.join(workspace, 'test_dem_slope.tif'))[0]
    aspect = rastertonumpy(osp.join(workspace, 'test_dem_aspect.tif'))[0]
    
    # save dem, slope, and aspect to .npz file   
    np.savez(osp.join(workspace, 'test_ras.npz'), 
             dem=dem, slope=slope, aspect=aspect) 
    
    # save to .mat file         
    param_dict = {'dem': dem, 'slope': slope, 'aspect': aspect}
    scipy.io.savemat(osp.join(workspace, 'test_ras.mat'), param_dict)

    
#def rastertonumpy(gisraster):
#    '''
#    Converts gisraster to numpyarray and outputs parameters used to later convert it back
#    '''
#    left = float(str(arcpy.GetRasterProperties_management(gisraster, 'LEFT')))
#    bottom = float(str(arcpy.GetRasterProperties_management(gisraster, 'BOTTOM')))
#    cellsize = float(str(arcpy.GetRasterProperties_management(gisraster, 'CELLSIZEX')))
#
#    nparray = arcpy.RasterToNumPyArray(gisraster)
#
#    rows = np.size(nparray, 0)
#    cols = np.size(nparray, 1)
#    
#    return [nparray, rows, cols, left, bottom, cellsize]
#
#    
#def numpytoraster(npraster, gisraster, left, bottom, cellsize, projection):
#    '''
#    Converts a numpyraster to a gisraster using the inputs from the rastertonumpy function
#    '''
#    array = arcpy.NumPyArrayToRaster(npraster, arcpy.Point(left, bottom), cellsize, cellsize)
#    array.save(gisraster)
#    arcpy.DefineProjection_management(gisraster, projection)
#    
#    return gisraster