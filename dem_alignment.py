import numpy as np
import scipy, scipy.interpolate, scipy.stats
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import lmfit
import seaborn as sns
import pandas as pd

class DEM_Aligner(object):
    '''
    Class to correct 3D shifts between two DEMs
    
    described by:
    Nuth, C., and A. Kaeaeb (2011), Co-registration and bias corrections of
    satellite elevation data sets for quantifying glacier thickness change,
    Cryosphere, 5(1), 271-290.
    '''    

    def __init__(self, Z1, Z2=None, px_res=None, mask=None):
        
        '''load data and set attributes

        parameters
        -------
        Z1: 
            reference DEM (2d numpy array)
        
        Z2: 
            DEM to be shifted, must have identical shape as Z1
        
        px_res: 
            pixel resolution in meters
        
        mask: 
            grid to mask out areas, must have identical shape as Z1 (pixels to 
            be masked must be np.nan valued, remaining pixels must have value 1)
        '''
        
        # if no mask is provided, create one with all ones
        if mask is None:
            mask = np.ones_like(Z1)
        
        # if only Z1 is provided, Z2 will be copy of Z1
        if Z2 is None:
            Z2 = Z1.copy()
        
        if px_res is None:
            px_res = 1
            
        # calculate DEM slope
        [dy, dx] = np.gradient(Z1, px_res, px_res)
        slope = np.arctan(np.hypot(dx, dy))
        
        # multiplication with -1 since y-origin is up      
        azimuth = np.arctan2(-1 * dy, dx) + np.pi
        
        # create geographic azimuths (clockwise orientation, N = 0 deg)
        azimuth = np.mod(-1 * azimuth + np.pi / 2.0, np.pi * 2.0)
    
        # mask to exclude slopes below a minimum slope 
        # dh/tan(slope) approaches infinity 
        fltr = (slope >= np.arctan(0.05))
                            
        self.Z1 = Z1 * mask
        self.Z2 = Z2
        self.Z2_orig = Z2
        self.px_res = float(px_res)
        self.mask = mask
        
        # pixel coordinates
        self.x_coords = np.arange(0, Z1.shape[1])
        self.y_coords = np.arange(0, Z1.shape[0])
        
        self.dx = dx
        self.dy = dy
        self.slope = slope
        self.azimuth = azimuth
        
        self.fltr = fltr
        self.azimuth_orig = azimuth
        self.slope_orig = slope
        
        # the following attributes get filled in run_alignment
        self.dh = None
        self.dh_1d = None 
        self.azimuth_1d = None
        self.tan_slope = None
        self.slope_1d = None
        
        self.params = None

        self.shifts = None 
        
        self.rmses = None
        
        self.best_x_shift = None
        self.best_y_shift = None
        self.best_z_shift = None
        self.solutions_df = None
                
    def eq3(self, a, b, c, aspect, tan_slope):
        '''
        eq3 in nuth and kaeaeb (2011)
        the amplitude of the cosine (a) is directly the magnitude of the shift 
        vector, b is the direction of the shift vector and c is the mean bias 
        between the DEMs divided by the mean slope tangent of the terrain
        '''
        dh = (a * np.cos(b - aspect) + c) * tan_slope 
        return dh        
    
    def residuals(self, params, aspect, tan_slope, dh_meas): 
        parvals = params.valuesdict()
        
        a = parvals['a']
        b = parvals['b']
        c = parvals['c']
                             
        dh = self.eq3(a, b, c, aspect, tan_slope)
        
        # return residuals (to be minimized)    
        residuals = dh_meas - dh         
            
        return residuals
        
    def shift_DEM(self, x_shift, y_shift, z_shift, method='griddata'):        
        '''
        shifts DEM and interpolates onto the coordinates of reference DEM

        parameters
        -------
        x_shift: 
            x_shift in meters
        
        y_shift:
            y_shift in meters
        
        z_shift: 
            z_shift in meters
        
        method: 
            'griddata': slowest method, but can deal with nans in input elevations 
            
            'interp2d': fast, but canNOT deal with nans in input elevations (i.e., masked DEMs) 
            
            'RectBivariateSpline': fastest, but canNOT deal with nans in input elevations (i.e., masked DEMs)
        '''
               
        # interpolation occurs in pixel coordinates, hence divide x and y shifts by map resolution
        x_shift_px = x_shift / self.px_res 
        y_shift_px = y_shift / self.px_res
        
        x_coords_moved = self.x_coords + x_shift_px
        # minus since grid has origin in upper left
        y_coords_moved = self.y_coords - y_shift_px
        z_coords_moved = self.Z2_orig + z_shift
        
        print 'Interpolation method used: {}'.format(method) 
        
        if method == 'griddata':
            '''slowest method, but can deal with nans in input elevs'''
        
            # create meshgrids for shifted coordinates
            x_coords_moved, y_coords_moved = np.meshgrid(x_coords_moved, y_coords_moved)
            
            # flatten into 1d vectors       
            x_coords_moved = x_coords_moved.ravel()
            y_coords_moved = y_coords_moved.ravel()
            z_coords_moved = z_coords_moved.ravel()
            
            # filter to eliminate nans
            fltr_nan = z_coords_moved != np.isnan 
            
            # create meshgrids for target coordinates
            x_coords_target, y_coords_target = np.meshgrid(self.x_coords, self.y_coords)
            
            Z2 = scipy.interpolate.griddata((x_coords_moved[fltr_nan], y_coords_moved[fltr_nan]), 
                                            z_coords_moved[fltr_nan], (x_coords_target, 
                                            y_coords_target), method='linear')
                                            
        elif method == 'interp2d':
            '''
            fast, but canNOT deal with nans in input elevs (i.e., masked DEMs)
            '''
                                            
            interpolator = scipy.interpolate.interp2d(x_coords_moved, y_coords_moved, 
                                                      z_coords_moved, kind='linear') #cubic
                                                      
            # interpolate onto the coordinates from the reference DEM
            Z2 = interpolator(self.x_coords, self.y_coords)
        

        elif method == 'RectBivariateSpline':
            '''
            fastest, but canNOT deal with nans in input elevs (i.e., masked DEMs)
            '''
            
            # x and y coords are swapped
            interpolator = scipy.interpolate.RectBivariateSpline(y_coords_moved, 
                                                                 x_coords_moved, 
                                                                 z_coords_moved) 
            
            # interpolate onto the coordinates from the reference DEM
            Z2 = interpolator(self.y_coords, self.x_coords)
        
        else:
            raise ValueError('Interpolation method {} not available - use griddata '
                              'interp2d, or RectBivariateSpline'.format(method))

        # mask out the pixels that lack coverage due to the DEM shift
        # important to get correct results
        if x_shift < 0: # movement to the west (left) - set to nan on the right side
            Z2[:, int(np.floor(x_shift_px)):] = np.nan 
        else: # set to nan on the left side
            Z2[:, :int(np.ceil(x_shift_px))] = np.nan
            
        if y_shift < 0: # movement to south (bottom) - set to nan at the top
            Z2[:int(np.ceil(np.abs(y_shift_px))), :] = np.nan
        else: # set to nan at the bottom
            Z2[int(-np.ceil(y_shift_px)):, :] = np.nan
        
        # check whether output is all nans and raise error if this is the case
        if np.isnan(np.nanmin(Z2)) == 1:
            raise ValueError('Shifted DEM contains nans only - try the griddata '
                             'interpolation method')
            
        return Z2        
                   
    def run_alignment(self, max_iterations=1, delta_rmse=0.001, fitting_method='differential_evolution', 
                      interpolation_method='griddata'):                          
        '''
        runs curve fitting and shifts DEM

        parameters
        -------
        max_iterations:
            max number of iterations (one iteration includes curve fitting and DEM shifting)
        
        delta_rmse: 
            minimal improvement of rmse required to continue iterations
            a delta_rmse of 0 guarantees that iterations stop in case rmse increases
        
        fitting_method: 
            used for curve fitting. must be in ['leastsq', 'least_squares', 
            'differential_evolution', 'basinhopping', 'ampgo', 'nelder', 'lbfgsb', 'powell']
            'differential_evolution' tends to work best

        interpolation_method: 
            used for DEM shifting 
            
            'griddata': slowest method, but can deal with nans in input elevs
            
            'interp2d': fast, but canNOT deal with nans in input elevs (i.e., masked DEMs)
            
            'RectBivariateSpline': fastest, but canNOT deal with nans in input elevs (i.e., masked DEMs)
        '''
        
        if fitting_method not in ['leastsq', 'least_squares', 'differential_evolution', 
        'basinhopping', 'ampgo', 'nelder', 'lbfgsb', 'powell']:
            
            raise Warning('{} is not a valid fitting method, using differential_evolution' 
                          ' instead'.format(fitting_method))
            
            fitting_method = 'differential_evolution'
        
        # set up parameters for the minimization
        params = lmfit.Parameters()
        
        params.add('a', value=0, min=-1000.0, max=1000.0)
        params.add('b', value=0, min=0, max=np.pi * 2)
        params.add('c', value=0, min=-50.0, max=50.0) # vary=False
        
        self.params = params

        self.Z2 = self.Z2_orig

        total_x_shift = 0
        total_y_shift = 0
        total_z_shift = 0
        
        self.rmses = []
        self.shifts = {'x':[], 'y':[], 'z':[]} 
        
        # lists for cumulative shifts
        total_x_shifts = []
        total_y_shifts = []
        total_z_shifts = []
        
        for counter in range(max_iterations):
            
            self.Z2 = self.Z2 * self.mask
            
            # elevation difference
            self.dh = self.Z1 - self.Z2          
            
            # remove nans and pixels across flat slopes (fltr)
            fltr_combined = self.fltr & ~np.isnan(self.dh)
            
            self.dh_1d = self.dh[fltr_combined]  
            self.azimuth_1d = self.azimuth_orig[fltr_combined]
            self.slope_1d = self.slope_orig[fltr_combined]
            self.tan_slope = np.tan(self.slope_1d)
            
            if counter > 0:
                
                nrm = self.dh_1d / self.tan_slope
                
                fltr_add = np.abs(nrm - np.mean(nrm)) < (10 * np.std(nrm))
                
                self.dh_1d = self.dh_1d[fltr_add] 
                self.azimuth_1d = self.azimuth_1d[fltr_add] 
                self.slope_1d = self.slope_1d[fltr_add] 
                self.tan_slope = self.tan_slope[fltr_add] 
                
            minimizer_object = lmfit.Minimizer(self.residuals, self.params, 
                                               fcn_args=(self.azimuth_1d, 
                                               self.tan_slope, self.dh_1d))
                                                      
            # run minimization                                 
            output = minimizer_object.minimize(method=fitting_method)
            
            # make sure script runs for lmfit versions older/newer than version 0.9
            # older than 0.9 
            if output == 1:
                fitted_parameters = minimizer_object.params
                rmse = round((np.mean(minimizer_object.residual ** 2)) ** 0.5, 5)
            
            # newer than 0.9
            else:
                fitted_parameters = output.params
                rmse = round((np.mean(output.residual ** 2)) ** 0.5, 5)
                            
            # extract best parameter value for each parameter
            best_a = fitted_parameters['a'].value
            best_b = fitted_parameters['b'].value
            best_c = fitted_parameters['c'].value   
                
            # parse out shift vector
            x_shift = best_a * np.sin(best_b)
            y_shift = best_a * np.cos(best_b)
            z_shift = best_c * np.tan(np.mean(self.slope_1d)) 
            
            if counter == 0:
                plt.figure(facecolor='white')
            
            fit = self.eq3(best_a, best_b, best_c, np.deg2rad(np.arange(0, 360)), 1)
            
            plt.plot(np.rad2deg(self.azimuth_1d), self.dh_1d / self.tan_slope, '.', 
                     alpha=0.5, label='run ' + str(counter + 1))
            plt.plot(np.arange(0, 360), fit, '-', alpha=0.5, lw=2, 
                     label='fit run ' + str(counter + 1))
                                        
            print '----------------------------------------------------------' 
            
            print 'Iteration: {}'.format(counter + 1)
            
            print 'Shift North: {} m = {} px'.format(round(y_shift, 3), 
                                                round(y_shift / self.px_res, 3))
                                                
            print 'Shift East: {} m = {} px'.format(round(x_shift, 3), 
                                                round(x_shift / self.px_res, 3))
                                                
            print 'Shift Up: {} m'.format(round(z_shift, 3))  
            
            print 'RMSE: {} m'.format(rmse) 
            #--------------------------------------------------------------------------
                       
            self.rmses.append(rmse)
            self.shifts['x'].append(x_shift)
            self.shifts['y'].append(y_shift)
            self.shifts['z'].append(z_shift)
                                                                
            total_x_shift += x_shift
            total_y_shift += y_shift
            total_z_shift += z_shift
            
            total_x_shifts.append(total_x_shift)
            total_y_shifts.append(total_y_shift)
            total_z_shifts.append(total_z_shift)

            # call shift method - all shifts are in m
            # start with original Z2 DEM and shift by total accumulates shifts to avoid
            # artifacts from multiple interpolation runs
            Z2 = self.shift_DEM(total_x_shift, total_y_shift, total_z_shift, 
                                method=interpolation_method)
                                                                                                                                                                    
            # update Z2                                        
            self.Z2 = Z2
            
            # check whether rmse improved
            if counter > 0:
                rmse_improvement = self.rmses[-2] - self.rmses[-1]
                if rmse_improvement < delta_rmse:
                    print 'RMSE improvement ({} m) smaller than defined '\
                    'threshold ({} m): stop alignment process'.format(rmse_improvement, delta_rmse)
                    break
                
                
        print '----------------------------------------------------------'
        
        print 'Checking whether the last iteration generated the best solution ' \
              '(in terms of the rmse)'  
                
        # write everything into a dataframe        
        solutions_df = pd.DataFrame({'rmse': self.rmses, 'x_shift': total_x_shifts,
                                      'y_shift': total_y_shifts, 
                                      'z_shift': total_z_shifts})
        
        # find the iteration with the lowest rmse                              
        best_index = solutions_df['rmse'].idxmin()
        self.best_x_shift = solutions_df.loc[best_index, 'x_shift']
        self.best_y_shift = solutions_df.loc[best_index, 'y_shift']
        self.best_z_shift = solutions_df.loc[best_index, 'z_shift']
        self.solutions_df = solutions_df

        
        if best_index != counter: 
            
            print 'Best solution (iter {}) is NOT from the last iteration (iter {}) ->' \
            ' reinterpolate best solution'.format(best_index + 1, counter + 1)
        
            # call shift method - all shifts are in m
            Z2 = self.shift_DEM(self.best_x_shift, self.best_y_shift, self.best_z_shift, 
                                method=interpolation_method)
                                                                                                                                                                    
            # update Z2                                        
            self.Z2 = Z2

        else:
            print 'Best solution (iter {}) is from the last iteration (iter {}) ->' \
            ' no need for reinterpolation'.format(best_index + 1, counter + 1)
            
        #----------------------------------------------------------------------
            
        plt.gca().set(xlim=[0, 360], xlabel='Terrain aspect', ylabel='dh/tan(slope)')
        plt.legend()
                               
        print '----------------------------------------------------------'
        
        print 'Best solution achieved in iteration {}'.format(best_index + 1)        
                
        print 'Total shift North: {} m = {} px'.format(round(self.best_y_shift, 3), 
                                            round(self.best_y_shift / self.px_res, 3))        
                                                    
        print 'Total shift East: {} m = {} px'.format(round(self.best_x_shift, 3), 
                                            round(self.best_x_shift / self.px_res, 3))
                                            
        print 'Total shift Up: {} m'.format(round(self.best_z_shift, 3))
               
    def plot_results(self, cbarlim=0):        
        '''
        plots slope, azimuth, elevation difference before shift, 
        elevation difference after shift, elevation difference distribution before shift,
        elevation difference distribution after shift
        
        parameters
        -------
        cbarlim: 
            limits of delta elevation colorbar in meters
        '''
    
        fig, ax = plt.subplots(3, 2, facecolor='white')
        
        # slope and azimuth plots
        slope = ax[0, 0].imshow(np.rad2deg(self.slope), interpolation=None, 
                                vmin=0, vmax=90, cmap='Greys')
        divider = make_axes_locatable(ax[0, 0])
        cax = divider.append_axes('right', size='5%', pad=0.05)
        fig.colorbar(slope, cax=cax)
        ax[0, 0].set_title('Slope (degrees)')

        azimuth = ax[0, 1].imshow(np.rad2deg(self.azimuth), interpolation=None, 
                                  vmin=0, vmax=360, cmap='Greys')
        divider = make_axes_locatable(ax[0, 1])
        cax = divider.append_axes('right', size='5%', pad=0.05)
        fig.colorbar(azimuth, cax=cax)
        ax[0, 1].set_title('Azimuth (degrees)')
        
        # elevation difference plots
        dh_orig = self.Z1 - self.Z2_orig
        dh_orig = dh_orig[~np.isnan(dh_orig)]
        
        dh_new = self.Z1 - self.Z2
        dh_new = dh_new[~np.isnan(dh_new)]
        
        # determine limits for colorbar
        if cbarlim == 0:
            cbarlim = np.max([np.abs(np.min([np.min(dh_orig), np.min(dh_new)])), 
                          np.abs(np.max([np.min(dh_orig), np.min(dh_new)]))])
        
        diff1 = ax[1, 0].imshow(self.Z1 - self.Z2_orig, cmap='RdBu', 
                        interpolation=None, vmin=-cbarlim, vmax=cbarlim)
        divider = make_axes_locatable(ax[1, 0])
        cax = divider.append_axes('right', size='5%', pad=0.05)
        fig.colorbar(diff1, cax=cax)
        ax[1, 0].set_title('Elevation difference (m) before shift')
        
        diff2 = ax[1, 1].imshow(self.Z1 - self.Z2, cmap='RdBu', interpolation=None, 
                          vmin=-cbarlim, vmax=cbarlim)
        divider = make_axes_locatable(ax[1, 1])
        cax = divider.append_axes('right', size='5%', pad=0.05)
        fig.colorbar(diff2, cax=cax)
        ax[1, 1].set_title('Elevation difference (m) after shift')
        
        # normalize with tangent of the slope
        dh_nm_orig = (self.Z1 - self.Z2_orig) / np.tan(self.slope)    
        dh_nm_orig = dh_nm_orig[self.fltr & ~np.isnan(dh_nm_orig)]
        
        dh_nm_new = (self.Z1 - self.Z2) / np.tan(self.slope)
        dh_nm_new = dh_nm_new[self.fltr & ~np.isnan(dh_nm_new)]
                
        # remove outliers (defined as points that are more than 3 standard devs from mean)
        # do this in an iterative fashion
        for r in range(0, 3):
            dh_nm_orig = dh_nm_orig[np.abs(dh_nm_orig - np.mean(dh_nm_orig)) < 
                                    (3 * np.std(dh_nm_orig))]           
            dh_nm_new = dh_nm_new[np.abs(dh_nm_new - np.mean(dh_nm_new)) < 
                                    (3 * np.std(dh_nm_new))]
        
        # distribution plots
        sns.distplot(dh_nm_orig, ax=ax[2, 0], kde=False, fit=scipy.stats.norm)
        sns.distplot(dh_nm_new, ax=ax[2, 1], kde=False, fit=scipy.stats.norm)
                            
        xlim = [np.nanmin(dh_nm_orig), np.nanmax(dh_nm_orig)]
        ax[2, 0].set_xlim(xlim)
        ax[2, 1].set_xlim(xlim)
        
        ymax = np.max([ax[2, 0].get_ylim()[1], ax[2, 1].get_ylim()[1]])
        
        if ymax <= 1:
            ax[2, 0].set_ylim([0, ymax])
            ax[2, 1].set_ylim([0, ymax])
        
        ax[2, 0].set_title('Normalized elevation differences (m) before shift')
        ax[2, 1].set_title('Normalized elevation differences (m) after shift')
        
        fig.tight_layout()        
        plt.show()    
        
        return [fig, ax]