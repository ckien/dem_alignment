# DEM_alignment

### About

Python tool to remove 3D shifts between DEMs.

Described by: Nuth, C., and A. Kaeaeb (2011), Co-registration and bias corrections of satellite elevation data sets for quantifying glacier thickness change, The Cryosphere, 5(1), 271-290.

Key dependenices: numpy, pandas, lmfit (for curve fitting), scipy (for DEM interpolation), gdal (geotiff import and export), and seaborn (for plotting).

Written and tested in Python 2.7.




### Example 1: Test with artificially shifted DEM ###

```python
Z1 = np.load('./data/test/test_ras.npz')['dem']
px_res = 5.0
    
# create an instance of the DEM_Aligner class
dem_aligner = dem_alignment.DEM_Aligner(Z1, px_res=px_res)
    
# randomly sample shifts
x_shift_prescribed = random.sample(np.arange(-20, 20, 0.1), 1)[0]
y_shift_prescribed = random.sample(np.arange(-20, 20, 0.1), 1)[0]
z_shift_prescribed = random.sample(np.arange(-10, 10, 0.1), 1)[0]
    
# apply these shifts to the original DEM
Z2 = dem_aligner.shift_DEM(x_shift=x_shift_prescribed, y_shift=y_shift_prescribed, 
z_shift=z_shift_prescribed, method='griddata')
    
# create a new instance of the DEM_Aligner class, using original and shifted DEM as input
dem_aligner = dem_alignment.DEM_Aligner(Z1, Z2, px_res)
    
# run alignment     
dem_aligner.run_alignment(max_iterations=10, delta_rmse=0.0,
fitting_method='differential_evolution', interpolation_method='griddata')
  
# plot results
[fig, ax] = dem_aligner.plot_results() 
       
tot_xshift = dem_aligner.total_x_shift
tot_yshift = dem_aligner.total_y_shift
tot_zshift = dem_aligner.total_z_shift
    
# compare prescribed and corrected shifts (ratio should be -1.0)
print '----------------------------------------------------------'
print 'ratio x: {}'.format(x_shift_prescribed / tot_xshift)
print 'ratio y: {}'.format(y_shift_prescribed / tot_yshift)
print 'ratio z: {}'.format(z_shift_prescribed / tot_zshift)
```



### Example 2: Determine shift between two different DEMs ###

```python
# load original DEMs and mask
[Z1, rasterobj] = util.rastertonumpy(osp.join(workspace, 'reference.tif'))
Z2 = util.rastertonumpy(osp.join(workspace, 'WV_20130728.tif'))[0]
mask = util.rastertonumpy(osp.join(workspace, 'mask.tif'))[0].astype('float32')
cellsize = rasterobj.GetGeoTransform()[1]

# create an instance of the DEM_Aligner class, using the two DEMs as input
dem_aligner = dem_alignment.DEM_Aligner(Z1, Z2, cellsize, mask)

# run alignment, set maximum number of iterations, rmse improvement threshold, 
# method for curve fitting, and method for DEM interpolation while shifting
dem_aligner.run_alignment(max_iterations=10, delta_rmse=0.0,
                          fitting_method='differential_evolution',
                          interpolation_method='griddata')

# plot results, set limit of colorbars in dh map to +- 3 m
[fig, ax] = dem_aligner.plot_results(cbarlim=3)

# convert shifted DEM back to geotiff
util.numpytoraster(osp.join(workspace, 'WV_20130728_shifted.tif'), dem_aligner.Z2, 
                   rasterobj)    
```
